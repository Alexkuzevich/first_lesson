<?php
# 1.1. Есть массив Имен и есть текст внутри которого присутсвуют имена. Найти количество раз, когда имя было ошибочно написано с маленькой буквы

$names_array = array("Alex", "Sam");
$text_message = "This are Alex and sam. Sam and alex are walking by a street";
$counter = 0;

for ($i = 0; $i < count($names_array); $i++) {
    $counter += substr_count($text_message, strtolower($names_array[$i]));
}

echo "<br/>"."Количество ошибочно написанных имен: ".$counter;

?>